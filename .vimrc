set nocompatible              " be iMproved, required
filetype off                  " required

" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

call plug#begin()
    Plug 'https://github.com/jeffkreeftmeijer/vim-numbertoggle'
    Plug 'stephpy/vim-yaml'
    Plug 'bling/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'https://github.com/tpope/vim-fugitive' "So awesome, it should be illegal 
    Plug 'sheerun/vim-polyglot'
""    Plug 'itchyny/lightline.vim'
    Plug 'ervandew/supertab'
    Plug 'raimondi/delimitmate'
    Plug 'preservim/nerdcommenter'
    Plug 'gryf/wombat256grf' 
    Plug 'morhetz/gruvbox'
    Plug 'mcmartelle/vim-monokai-bold'

call plug#end()


syntax on

colorscheme gruvbox
set background=dark
"colorscheme wombat256grf

set nu
set mouse=a

" folding
"set foldmethod=indent
"set foldlevel=1
"set foldclose=all
"set foldcolumn=2

" LightLine
set laststatus=2
set noshowmode
"let g:lightline = {}
"let g:lightline.colorscheme='darcula'

set wildmenu                        "display completion menu
set wildmode=list:longest,list:full "content of completion menu
"ignore some extensions while completing filename
"set wildignore=*.o,*.so,,*.gz,*.bz2,*.tar,*.tgz,*.tbz2,*.png,*.jpg,*.jpeg,*.gif

"Supertab

"Syntastic
"let g:syntastic_auto_loc_list = 1
" Redefine errors/warning symbols on sidebar
"let g:syntastic_error_symbol = 'EE'
"let g:syntastic_style_error_symbol = 'ee'
"let g:syntastic_warning_symbol = 'WW'
"let g:syntastic_style_warning_symbol = 'Ws'
" redefine default python linter
"let g:syntastic_python_checker = 'pylint'
" add options to python linter
"let g:syntastic_python_checker_args = ''
"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 0

set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

"Indentation
"filetype indent on
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set autoindent
set smartindent
"set cindent
set pastetoggle=<F2>

"search settings
"set hlsearch "highlight searches
set nohlsearch
set incsearch "incremental searches
set ignorecase "ignore case in search patterns
set infercase "smart case support

"Airline
"let g:airline_powerline_fonts=1
let g:airline#extensions#tabline#enabled=1
let g:airline_theme='bubblegum'
let g:airline_section_z = ' %l / %L : %c '
